window.dataLayer = window.dataLayer || [];

function gtag() {
    dataLayer.push(arguments);
}
gtag('js', new Date());

jQuery(function($) {
    $('.slider').bgSwitcher({
        images: ['img/head2.png', 'img/head3.png', 'img/head1.png', 'img/head4.png'],
        interval: 4000,
        effect: "fade"
    });
});

jQuery(function($) {
    $('#gnav').slicknav();

    $(function() {
        $('a[href^=#]').click(function() {
            var speed = 500;
            var href = $(this).attr("href");
            var target = $(href == "#" || href == "" ? 'html' : href);
            var position = target.offset().top;
            $("html, body").animate({ scrollTop: position }, speed, "swing");
            return false;
        });
    });
});

$(function() {
    $(".acMenu .title").on("click", function() {
        $(this).next().slideToggle();
        $(this).toggleClass("active"); //追加部分
    });
});

$("#send_message").click(function() {
    if (!$("#name").val()) alert('名前が空欄です。');
    if (!$("#email").val()) alert('メールアドレスが空欄です。');
    if (!$("#tel").val()) alert('電話番号が空欄です。');
    if (!$("#message").val()) alert('メッセージが空欄です。');
    $.ajax({
        type: 'post',
        url: 'php/post_index_mail.php',
        data: {
            name: $("#name").val(),
            email: $("#email").val(),
            tel: $("#tel").val(),
            message: $("#message").val()
        },
        success: function(data) {
            if (!data == "OK") {
                alert('送信エラー 正常に送信できませんでした。');
            } else {
                alert('送信完了 お問い合わせありがとうございました。');
                $("#name").val("");
                $("#email").val("");
                $("#tel").val("");
                $("#message").val("");
            }
        }
    });
});